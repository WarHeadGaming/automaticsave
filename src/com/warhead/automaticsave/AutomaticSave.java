package com.warhead.automaticsave;

import org.spout.api.Spout;
import org.spout.api.plugin.CommonPlugin;
import org.spout.api.scheduler.TaskPriority;

/**
 *
 * @author Douglas
 */
public class AutomaticSave extends CommonPlugin{

    public String name = "[AutomaticSave]";
    private SaveTimer timer;
    
    @Override
    public void onEnable() {
        startSave();
        this.logInfo("Enabled");
        this.timer = new SaveTimer(this);
    }

    @Override
    public void onDisable() {
        this.logInfo("Disabled");
    }
    
    public void logInfo(String log){
        Spout.getLogger().info(name + " " + log);
    }
    
    public void startSave(){
        Spout.getScheduler().scheduleSyncRepeatingTask(this, new SaveTimer(this), 1000, 900000, TaskPriority.NORMAL);
    }
}
