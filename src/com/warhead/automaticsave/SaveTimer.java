package com.warhead.automaticsave;

import org.spout.api.Spout;

/**
 *
 * @author Douglas
 */
public class SaveTimer implements Runnable{

    private AutomaticSave plugin;
    
    public SaveTimer(AutomaticSave plugin){
        this.plugin = plugin;
    }
    @Override
    public void run() {
        plugin.logInfo("Autosaving world...");
        Spout.getEngine().getDefaultWorld().save();
        plugin.logInfo("...Done");
    }
}
